Here, you will find an overview of the open source informmation of this product. The detailed information can be found within the repository at [GitLab](https://gitlab.com/ourplant.net/products/s2-0022-measurement-head).

|document|download options|
|:-----|-----:|
|operating manual           |[de](https://gitlab.com/ourplant.net/products/s2-0022-measurement-head/-/raw/main/01_operating_manual/S2-0022_B_BA_Tatctile%20Measurement%20Head.pdf)|
|assembly drawing           |[de](https://gitlab.com/ourplant.net/products/s2-0022-measurement-head/-/raw/main/02_assembly_drawing/s2-0022_C_ZNB_measurement_head.pdf)|
|circuit diagram            |[de](https://gitlab.com/ourplant.net/products/s2-0022-measurement-head/-/raw/main/03_circuit_diagram/S2-0022-EPLAN-A.pdf)|
|maintenance instructions   |[de](https://gitlab.com/ourplant.net/products/s2-0022-measurement-head/-/raw/main/04_maintenance_instructions/S2-0022_A_WA_Measurement_Head.pdf)|
|spare parts                |[de](https://gitlab.com/ourplant.net/products/s2-0022-measurement-head/-/raw/main/05_spare_parts/S2-0022_A1_EVL.pdf)|

<!-- 2021 (C) Häcker Automation GmbH -->
