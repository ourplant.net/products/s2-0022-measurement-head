The S2-0022 Measurement Head has the following technical specifications.

## Technical information
| Dimensions in mm (W x D x H)           | 49 x 305 x 514 |
| :------------------------------------- | -------------- |
| Weight in kg                           | 3.8            |
| Operating pressure in bar              | 6              |
| Max. measured displacement in mm       | 30             |
| Resolution probe in µm                 | 1              |
| Max. permissible transverse force in N | 0.8            |

